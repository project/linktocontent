
The linktocontent module allows you to extend the TinyMCE editor (via Wysiwyg
API) with several plugins.

Plugins:
 - linktocontent_node.module: 
   Link to every node of every enabled type by browsing through your taxonomy structure
 - linktocontent_menu.module:
   Browse through your menu structure and link to menu items.
 - linktocontent_category.module:
   Extends linktocontent_node.module to use it if category.module is enabled.


Requirements
------------

This module and its plugins require Wysiwyg API installed and enabled.

Link to Node requires additionally Taxonomy module.

Link to Category requires Category module.


Installation
------------

1. Copy the folder named 'linktocontent' and its contents to the modules directory
   of your Drupal installation (for example 'sites/all/modules/'.

2. Go to 'admin/build/modules' and enable linktocontent. There are three plugins
   (for example Linktocontent_Node). You can enable any or all of these. 
   The Description column describes dependencies. 
   For example, to enable Linktocontent_Category you must enable Linktocontent_Node. 

3. Go to 'admin/settings/wysiwyg' and edit the profile you want to enable the
   new plugins (for example "linktonode") for.
   In section "buttons and plugins" check linktonode and save the profile.

4. Go to 'admin/settings/linktocontent' and enable every sub-module you would like to use.
   After enabling a module check its settings page for more specific settings (for example 
   which node types to display or which menus to use).


Support
-------

http://drupal.org/project/linktocontent

Author
------

Stefan Borchert http://drupal.org/user/36942/contact
