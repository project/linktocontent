
(function() {
  /* Import plugin specific language pack */
  tinymce.PluginManager.requireLangPack('linktonode', 'en,de,fr');

  tinymce.create('tinymce.plugins.LinkToNodePlugin', {
    getInfo : function() {
      return {
        longname : 'Link to node'
      }
    },

    init : function(ed, url) {
      ed.addCommand('mceLinktonode', function() {
        ed.windowManager.open({
          file : Drupal.settings.basePath + 'index.php?q=linktocontent/node',
          width : 400 + parseInt(ed.getLang('linktonode.delta_width', 0)),
          height : 295 + parseInt(ed.getLang('linktonode.delta_height', 0))
        }, {
          plugin_url : url // Plugin absolute URL
        });
      });
        
      ed.addButton('linktonode', {
        title : 'linktonode.image_desc',
        cmd : 'mceLinktonode',
        image : url + '/images/linktonode.gif'
      });

      ed.onNodeChange.add(function(ed, cm, n, co) {
        cm.setDisabled('linktonode', co && n.nodeName != 'A');
        cm.setActive('linktonode', n.nodeName == 'A' && !n.name);
      });
    }
  });
  tinymce.PluginManager.add("linktonode", tinymce.plugins.LinkToNodePlugin);
})();
