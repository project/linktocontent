// French lang variables

tinyMCE.addToLang('linktonode',
{
    title : 'lien vers noeud',
    loading : 'chargement...',
    insert_title : 'Ins&egrave;re un lien vers un noeud',
    image_desc : 'Ins&egrave;re un lien vers un noeud',
    browse_tab : 'parcourir',
    search_tab : 'rechercher',
    browse_title : 'parcourir les documents',
    search_title : 'trouver les documents',
    label_documents : 'documents',
    thead_title : 'titre du noeud',
    thead_date : 'cr&eacute;&eacute;',
    thead_author : 'auteur',
    choose_category : '<!-- choisir une categorie -->'
});