// English lang variables

tinyMCE.addI18n('en.linktonode',
{
    title : 'link to node',
    loading : 'loading...',
    insert_title : 'Insert link to node',
    image_desc : 'Insert link to node',
    browse_tab : 'browse',
    search_tab : 'search',
    browse_title : 'browse documents',
    search_title : 'find documents',
    label_documents : 'documents',
    thead_title : 'node title',
    thead_date : 'created',
    thead_author : 'author',
    choose_category : '<!-- choose category -->'
});