
(function() {
  /* Import plugin specific language pack */
  tinymce.PluginManager.requireLangPack('linktomenu', 'en,de,fr');

  tinymce.create('tinymce.plugins.LinkToMenuPlugin', {
    getInfo : function() {
      return {
        longname : 'Link to menu'
      }
    },

    init : function(ed, url) {
      ed.addCommand('mceLinktomenu', function() {
        ed.windowManager.open({
          file : Drupal.settings.basePath + 'index.php?q=linktocontent/menu',
          width : 400 + parseInt(ed.getLang('linktomenu.delta_width', 0)),
          height : 295 + parseInt(ed.getLang('linktomenu.delta_height', 0))
        }, {
          plugin_url : url // Plugin absolute URL
        });
      });

      ed.addButton('linktomenu', {
        title : 'linktomenu.image_desc',
        cmd : 'mceLinktomenu',
        image : url + '/images/linktomenu.gif'
      });

      ed.onNodeChange.add(function(ed, cm, n, co) {
        cm.setDisabled('linktomenu', co && n.nodeName != 'A');
        cm.setActive('linktomenu', n.nodeName == 'A' && !n.name);
      });
    }
  });
  tinymce.PluginManager.add("linktomenu", tinymce.plugins.LinkToMenuPlugin);
})();
