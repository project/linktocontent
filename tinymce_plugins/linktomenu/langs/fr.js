// French lang variables

tinyMCE.addI18n('fr.linktomenu',
{
    title : 'lien vers noeud',
    loading : 'chargement...',
    insert_title : 'Ins&egrave;re un lien vers un &eacute;l&eacute;ment de menu',
    image_desc : 'Ins&egrave;re un lien vers un &eacute;l&eacute;ment de menu',
    browse_tab : 'parcourir',
    search_tab : 'rechercher',
    browse_title : 'parcourir les &eacute;l&eacute;ments de menu',
    search_title : 'trouver les &eacute;l&eacute;ments de menu',
    label_documents : '&eacute;l&eacute;ments de menu',
    thead_title : 'titre',
    thead_date : 'cr&eacute;&eacute;',
    thead_author : 'auteur',
    choose_category : '<!-- choisir un menu -->'
});