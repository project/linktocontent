// German lang variables

tinyMCE.addI18n('de.linktomenu', {
    title : 'Verweis zu Men&uuml;punkt',
    loading : 'Daten laden...',
    insert_title : 'Verweis zu Men&uuml;punkt einf&uuml;gen',
    image_desc : 'Verweis zu Men&uuml;punkt einf&uuml;gen',
    browse_tab : 'Men&uuml;struktur',
    search_tab : 'suchen',
    browse_title : 'Men&uuml;struktur durchsuchen',
    search_title : 'nach Men&uuml;punkt suchen',
    label_documents : 'Men&uuml;punkte',
    thead_title : 'Titel',
    choose_category : '<-- Men&uuml; waehlen -->'
});
