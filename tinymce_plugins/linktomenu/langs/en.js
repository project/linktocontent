// English lang variables

tinyMCE.addI18n('en.linktomenu',
{
    title : 'link to menu item',
    loading : 'loading...',
    insert_title : 'Insert link to menu item',
    image_desc : 'Insert link to menu item',
    browse_tab : 'browse',
    search_tab : 'search',
    browse_title : 'browse menu items',
    search_title : 'find menu items',
    label_documents : 'menu items',
    thead_title : 'title',
    choose_category : '<!-- choose menu -->'
});